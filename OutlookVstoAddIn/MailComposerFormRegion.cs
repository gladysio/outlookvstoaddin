﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Office = Microsoft.Office.Core;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace OutlookVstoAddIn
{
    partial class MailComposerFormRegion
    {
        #region Fabrique de zones de formulaire 

        [Microsoft.Office.Tools.Outlook.FormRegionMessageClass(Microsoft.Office.Tools.Outlook.FormRegionMessageClassAttribute.Note)]
        [Microsoft.Office.Tools.Outlook.FormRegionName("OutlookVstoAddIn.MailComposerFormRegion")]
        public partial class MailComposerFormRegionFactory
        {
            // Se produit avant l'initialisation de la zone de formulaire.
            // Pour empêcher l'affichage de la zone de formulaire, définissez e.Cancel à true.
            // Utilisez e.OutlookItem pour obtenir une référence à l'élément Outlook actuel.
            private void MailComposerFormRegionFactory_FormRegionInitializing(object sender, Microsoft.Office.Tools.Outlook.FormRegionInitializingEventArgs e)
            {
            }
        }

        #endregion

        // Se produit avant l'affichage de la zone de formulaire.
        // Utilisez this.OutlookItem pour obtenir une référence à l'élément Outlook actuel.
        // Utilisez this.OutlookFormRegion pour obtenir une référence à la zone de formulaire.
        private void MailComposerFormRegion_FormRegionShowing(object sender, System.EventArgs e)
        {
        }

        // Se produit à la fermeture de la zone de formulaire.
        // Utilisez this.OutlookItem pour obtenir une référence à l'élément Outlook actuel.
        // Utilisez this.OutlookFormRegion pour obtenir une référence à la zone de formulaire.
        private void MailComposerFormRegion_FormRegionClosed(object sender, System.EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Outlook.MailItem mail = this.OutlookItem as Outlook.MailItem;
            if (mail!=null)
            {
                dynamic userData = JsonConvert.DeserializeObject(Globals.ThisAddIn.api.data.dataUser);
                mail.Body += "\n\n"+userData.personalUri;
            }
        }
    }
}

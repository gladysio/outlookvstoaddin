﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace OutlookVstoAddIn
{
    // this class centralize the calls to the myGladys API
    public class MyGladysApi
    {
        #region private properties
        private bool local = false;
        private static readonly WebClient client = new WebClient();
        #endregion
        public Data data = new Data();
        #region nested classes
        private class GladysContact
        {
            public string email { get; set; }
            public bool mandatory { get; set; }
            public int id { get; set; }
        }
        [Serializable]
        public class Data
        {
            public Data() { }
            public string Email { get; set; }
            [XmlIgnore]
            public string Password { get; set; }
            public string dataUser { get; set; }
            public int UserId { get; set; }
            public bool IsLogged { get; set; }
            [XmlIgnore]
            public Meeting meeting = new Meeting();
            [XmlIgnore]
            public List<Contact> contacts = new List<Contact>();
            [XmlIgnore]
            public List<Contact> allContacts = new List<Contact>();
            [XmlIgnore]
            public Contact newContact;
            [XmlIgnore]
            public List<TimeSlot> timeSlots;
            [XmlIgnore]
            public JObject createdMeeting;
            [XmlIgnore]
            public string meetingId;
        }
        public class Meeting
        {
            public Meeting()
            {
                timezone = "Europe/Paris";
            }
            public string ownerId { get; set; }
            public string moment { get; set; }
            public string title { get; set; }
            public string place { get; set; }
            public string timezone { get; set; }
            public string description { get; set; }
            public string expectedTime { get; set; }
            public string period { get; set; }
            public string date { get; set; }
        };
        public class TimeSlot
        {
            public string start { get; set; }
            public string end { get; set; }
            public string date { get; set; }
            public int nbAbsent { get; set; }
            public string startHour { get; set; }
            public string endHour { get; set; }
            public override string ToString() { return String.Format("{0} De {1} à {2}", date, startHour, endHour); }
        };
        public class OutlookEvent
        {
            public string outlookId { get; set; }
            public string start { get; set; }
            public string end { get; set; }
            public bool valid { get; set; }

            public OutlookEvent(MeetingEvent ev)
            {
                DateTime utcStart = ev.start.ToUniversalTime();
                DateTime utcEnd = ev.end.ToUniversalTime();
                outlookId = ev.outlookId;
                start = utcStart.ToString("o");
                end = utcEnd.ToString("o");
            }
        };
        #endregion
        #region private methods
        private JObject post(string url, NameValueCollection data)
        {
            string responseInString = "";
            try
            {
                var response = client.UploadValues(url, "POST", data);
                responseInString = Encoding.UTF8.GetString(response);
                return JObject.Parse(responseInString);
            }
            catch (Exception e)
            {
                if (responseInString.ToLower() == "ok" || string.IsNullOrEmpty(responseInString))
                    return JObject.Parse(@"{ok:true}");
                MessageBox.Show(e.ToString(), "Erreur dans post", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
        // authentification
        private JObject auth(string email, string password)
        {
            var data = new NameValueCollection();
            data["email"] = email;
            data["password"] = password;
            return post("auth/login", data);
        }
        private JObject createMeeting(Meeting m)
        {
            var data = new NameValueCollection();
            data["ownerId"] = m.ownerId;
            data["kind"] = m.moment;
            data["title"] = m.title;
            data["place"] = m.place;
            data["timezone"] = m.timezone;
            data["expectedTime"] = m.expectedTime;
            data["date"] = m.date;
            return post("meetings/create", data);
        }
        private JObject addMeetingContacts(string userId, string meetingId, List<Contact> contacts)
        {
            List<GladysContact> locals = new List<GladysContact>();
            foreach (var c in contacts)
            {
                GladysContact gc = new GladysContact();
                gc.email = c.Email;
                gc.id = c.id;
                gc.mandatory = c.mandatory;
                locals.Add(gc);
            }
            var data = new NameValueCollection();
            data["users"] = JsonConvert.SerializeObject(locals);
            return post("meetings/members/" + meetingId + "/" + userId, data);
        }
        private JObject syncMeeting(string meetingId, string dataUser)
        {
            var data = new NameValueCollection();
            data["data"] = dataUser;
            return post("meetings/sync/" + meetingId, data);
        }
        private JObject findSlots(string meetingId, JObject o)
        {
            var data = new NameValueCollection();
            var json = JsonConvert.SerializeObject(o);
            data["meeting"] = json;
            return post("meetings/find/" + meetingId, data);
        }
        private JObject validateMeeting(TimeSlot ts)
        {
            var d = new NameValueCollection();
            d["start"] = ts.start;
            d["end"] = ts.end;
            d["timezone"] = (string)data.createdMeeting["timezone"];
            string debug = (string)data.createdMeeting["isOver"];
            // FIXIT pb with JS and c# convention for bool variables
            if (debug == "True")
                debug = "true";
            if (debug == "False")
                debug = "false";
            d["isOver"] = debug;
            d["id"] = data.UserId.ToString();
            d["user"] = data.dataUser;
            return post("meetings/selectdate/" + data.meetingId, d);
        }
        private string _sendMailsByGladys()
        {
            var response = client.UploadValues("meetings/" + data.meetingId + "/send/emails/" + data.UserId, "POST", new NameValueCollection());
            return Encoding.UTF8.GetString(response);

        }

        private JObject _addContact(string email)
        {
            var d = new NameValueCollection();
            d["contact"] = JsonConvert.SerializeObject(JObject.FromObject(new { id = data.UserId, email })); ;
            return post("contacts/create", d);
        }
        protected virtual void OnTimeSlotsFound(EventArgs e)
        {
            EventHandler handler = TimeSlotsFound;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        private void _createDraft()
        {
            var d = new NameValueCollection();
            d["data"] = JsonConvert.SerializeObject(data.createdMeeting);
            var ret = post("draft/create/" + data.meetingId, d);
        }


        #endregion
        #region specific private api calls for Outlook
        private void _createMeetingEvents(List<OutlookEvent> events)
        {
            if (events.Count == 0) return;
            var d = new NameValueCollection();
            var json = JsonConvert.SerializeObject(events);
            d["events"] = json;
            post("agendas/outlook/" + data.UserId, d);
        }
        private void _createMeetingEvent(OutlookEvent ev)
        {
            List<OutlookEvent> locals = new List<OutlookEvent>();
            locals.Add(ev);
            _createMeetingEvents(locals);
        }
        private void _deleteMeetingEvent(string appointmentId)
        {
            var d = new NameValueCollection();
            d["outlookId"] = appointmentId;
            var res = client.UploadValues("agendas/outlook/" + data.UserId, "DELETE", d);
            string responseInString = Encoding.UTF8.GetString(res);
        }
        private void _bindMeetingEvent(string gladysId, string outlookId)
        {
            var d = new NameValueCollection();
            d["meetingId"] = gladysId;
            d["outlookId"] = outlookId;
            client.UploadValues("meetings/outlook/bind", "POST", d);
        }
        private string _getMeetingEvents()
        {
            byte[] bytes = client.DownloadData("user/outlook/" + data.UserId);
            string download = Encoding.UTF8.GetString(bytes);
            return download;
        }
        #endregion
        #region events
        public event EventHandler TimeSlotsFound;
        #endregion
        #region public stuff (what is used by the other entities of the addin)
        public MyGladysApi()
        {
            client.BaseAddress = local ? "http://127.0.0.1:4000/api/" : "https://api.mygladys.fr/api/";

        }
        public void Login()
        {
            try
            {
                JObject o = auth(data.Email, data.Password);
                string userId = (string)o["data"]["userId"];
                JObject dataUser = (JObject)o["data"]["dataUser"];
                if (userId != null && userId != "")
                {
                    int iuserId;
                    data.IsLogged = (int.TryParse((string)o["data"]["userId"], out iuserId)) ? true : false;
                    if (data.IsLogged)
                    {
                        data.UserId = iuserId;
                        data.dataUser = JsonConvert.SerializeObject(dataUser);
                    }
                }
                else
                {
                    MessageBox.Show("Utilisateur inconnu", "Erreur dans Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Erreur dans CreateMeeting", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public bool CreateMeeting()
        {
            try
            {
                JObject createdMeeting = createMeeting(data.meeting);
                string meetingId = (string)createdMeeting["detail"]["id"];
                if (meetingId != null && meetingId != "")
                {
                    List<Contact> cts = new List<Contact>();
                    foreach (Contact ct in data.contacts)
                    {
                        Contact c = new Contact();
                        c.Email = ct.Email;
                        c.mandatory = false;
                        c.id = data.UserId;
                        cts.Add(c);
                    }
                    cts.Add(new Contact
                    {
                        Email = data.Email,
                        mandatory = true,
                        id = data.UserId
                    });
                    addMeetingContacts(data.UserId.ToString(), meetingId, cts);
                    JObject o = syncMeeting(meetingId, data.dataUser);
                    bool everybodyHasGladys = (bool)o["detail"];
                    if (everybodyHasGladys)
                    {
                        o = (JObject)createdMeeting["detail"];
                        o.Add(new JProperty("type", data.meeting.period));
                        o.Add(new JProperty("date", data.meeting.date));
                        data.createdMeeting = o;
                        data.meetingId = meetingId;
                        o = findSlots(meetingId, o);
                        data.timeSlots = ((JArray)o["detail"]).ToObject<List<TimeSlot>>();
                        //  WARNING!!!!WARNING!!!!WARNING!!!!
                        // the deserializer of Json.net transforms the dates represented as string
                        // the Z is removed!!!!!!
                        // so utc dates are transformed in Local dates
                        foreach (TimeSlot ts in data.timeSlots)
                        {
                            ts.start += "Z";
                            ts.end += "Z";
                        }
                        OnTimeSlotsFound(EventArgs.Empty);
                    }
                    else
                    {
                        _createDraft();
                        MessageBox.Show("Tous vos invités n'ont pas MyGladys, nous leur avons envoyé un message pour qu'ils partagent leurs disponibilités", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return true;
                    }
                }
                else
                {
                    MessageBox.Show("Tous vos invités n'ont pas myGladys", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Erreur dans selectTimeSlot", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return true;
            }
        }
        public bool selectTimeSlot(TimeSlot ts)
        {
            bool cancel = true;
            try
            {
                if ((MessageBox.Show("Vous êtes sur le point de créer un RV pour le " + ts.ToString() + ". Confirmation ?", "Confirmation",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes))
                {
                    //TODO: Stuff
                    validateMeeting(ts);
                    string format = "MM/dd/yyyy HH:mm:ssZ";
                    DateTime start = DateTime.ParseExact(ts.start, format, CultureInfo.InvariantCulture);
                    DateTime end = DateTime.ParseExact(ts.end, format, CultureInfo.InvariantCulture);
                    string outlookId = OutlookAppointmentHelper.AddMeetingEvent(new MeetingEvent
                    {
                        title = data.meeting.title,
                        location = data.meeting.place,
                        start = start,
                        end = end
                    });
                    // communicate the id of the outlook meeting event id to Gladys 
                    if (!OutlookAppointmentHelper.IsInternetAccessibleAgenda())
                        _bindMeetingEvent(data.meetingId, outlookId);
                    cancel = false; ;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Erreur dans selectTimeSlot", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return cancel;
        }
        public void sendMailsByGladys()
        {
            try
            {
                string rep = _sendMailsByGladys();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Erreur dans sendMailsByGladys", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public bool addContact(string email)
        {
            try
            {
                var ret = _addContact(email);
                return (ret == null) ? false : true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Erreur dans addContact", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        public bool deleteMeetingEvent(string globalAppointmentId)
        {
            try
            {
                _deleteMeetingEvent(globalAppointmentId);
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Erreur dans deleteMeetingEvent", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        public string getMeetingEvents()
        {
            try
            {
                return _getMeetingEvents();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Erreur dans getMeetingEvents", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "";
            }
        }
        public void synchronizeAgendas()
        {
            List<MeetingEvent> events = OutlookAppointmentHelper.getAppointments();
            try
            {
                if (events != null)
                {
                    List<OutlookEvent> olEvents = new List<OutlookEvent>();
                    foreach (MeetingEvent ev in events)
                    {
                        olEvents.Add(new OutlookEvent(ev));
                    }
                    _createMeetingEvents(olEvents);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Erreur dans synchronizeAgendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void addMeetingEvent(MeetingEvent ev)
        {

            try
            {
                _createMeetingEvent(new OutlookEvent(ev));
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Erreur dans addMeetingEvent", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void moreTimeSlotsBefore()
        {
            data.createdMeeting["date"] = data.timeSlots[0].start;
            data.createdMeeting["type"] = "before";
            JObject o = findSlots(data.meetingId, data.createdMeeting);
            List<TimeSlot> temp = ((JArray)o["detail"]).ToObject<List<TimeSlot>>();
            foreach (TimeSlot ts in temp)
            {
                ts.start += "Z";
                ts.end += "Z";
            }
            DateTime d = DateTime.ParseExact(temp[0].start, "MM/dd/yyyy HH:mm:ssZ", CultureInfo.InvariantCulture);
            List<TimeSlot> filteredList = new List<TimeSlot>();
            foreach (TimeSlot ts in temp)
            {
                if (DateTime.ParseExact(ts.start, "MM/dd/yyyy HH:mm:ssZ", CultureInfo.InvariantCulture) > DateTime.Now)
                    filteredList.Add(ts);
            }
            if (filteredList.Count == 0)
                return;
            data.timeSlots.InsertRange(0, filteredList);
            OnTimeSlotsFound(EventArgs.Empty);
        }
        public void moreTimeSlotsAfter()
        {
            data.createdMeeting["date"] = data.timeSlots[data.timeSlots.Count - 1].end;
            data.createdMeeting["type"] = "after";
            JObject o = findSlots(data.meetingId, data.createdMeeting);
            List<TimeSlot> temp = ((JArray)o["detail"]).ToObject<List<TimeSlot>>();
            foreach (TimeSlot ts in temp)
            {
                ts.start += "Z";
                ts.end += "Z";
            }
            DateTime d = DateTime.ParseExact(temp[0].start, "MM/dd/yyyy HH:mm:ssZ", CultureInfo.InvariantCulture);
            List<TimeSlot> filteredList = new List<TimeSlot>();
            foreach (TimeSlot ts in temp)
            {
                if (DateTime.ParseExact(ts.start, "MM/dd/yyyy HH:mm:ssZ", CultureInfo.InvariantCulture) > DateTime.Now)
                    filteredList.Add(ts);
            }
            if (filteredList.Count == 0)
                return;
            data.timeSlots.AddRange(filteredList);
            OnTimeSlotsFound(EventArgs.Empty);
        }
        #endregion
    }
}
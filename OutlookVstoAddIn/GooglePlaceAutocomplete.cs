﻿using GoogleApi;
using GoogleApi.Entities.Common.Enums;
using GoogleApi.Entities.Places.AutoComplete.Request;
using GoogleApi.Entities.Places.AutoComplete.Request.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace OutlookVstoAddIn
{
    class GooGlePlaceAutocomplete
    {
        public static List <string> get(string query)
        {
            List<string> results = new List<string>();
            try
            {
                if (string.IsNullOrEmpty(query))
                    return results;
                var request = new PlacesAutoCompleteRequest
                {
                    Key = "AIzaSyB9SMwSudXkapsJ5JxCBiaiytQVqJ-vRTo",
                    Input = query,
                };

                var response = GooglePlaces.AutoComplete.Query(request);
                if (response != null && response.Status == Status.Ok)
                {
                    var res = response.Predictions.ToList();
                    foreach (var r in res)
                    {
                        results.Add(r.Description);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return results;

        }
    }
}

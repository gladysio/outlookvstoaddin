﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using System.Collections.Specialized;
using System.Collections;
using System.Net.Mail;

namespace OutlookVstoAddIn
{
    public partial class MygladysTaskPane : UserControl
    {
        private MyGladysApi api;
        private MyGladysApi.Data data;
        public MygladysTaskPane()
        {
            data = Globals.ThisAddIn.api.data;
            api = Globals.ThisAddIn.api;
            InitializeComponent();
            tabControl1.Appearance = TabAppearance.FlatButtons;
            tabControl1.ItemSize = new Size(0, 1);
            tabControl1.SizeMode = TabSizeMode.Fixed;

            initHomePage();
            initConnectionPage();
            initMenuPage();
            initSharingPage();
            initMeetingDescriptionPage();
            initMeetingMomentsPage();
            initMeetingContactsPage();
            initTimeSlotsPage();
            initMoreTimeSlotsPage();
            initMeetingCreatedPage();

            if (data.IsLogged)
            {
                Connect();
            }
        }

        #region home page
        private void initHomePage()
        {
            button3.BackColor = GladysStyle.GreenButtonColor();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 1;
        }
        #endregion
        #region connection page
        private void initConnectionPage()
        {
            button5.BackColor = GladysStyle.GreenButtonColor();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "fabrice.mygladys@outlook.fr";
            textBox2.Text = "1cbda394ef04a2ea54362cc527a62124db550dbb";
            textBox2.Text = "5f6ae9c80966f695c84b64dceabb6db7f88755f2";

        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "mygladys@groupe-pegasus.com";
            textBox2.Text = "der58gvb69!";
        }
        private void button6_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;
        }
        private void button5_Click(object sender, EventArgs e)
        { 
            try
            {
                MailAddress email = new MailAddress(textBox1.Text);
                if (textBox1.Text != "")
                {
                    if (OutlookHelper.hasLocalAccount(textBox1.Text))
                    {
                        data.Email = textBox1.Text;
                        data.Password = textBox2.Text;
                        api.Login();
                        label5.Text = data.Email;
                        textBox1.Text = "";
                        textBox2.Text = "";
                        if (data.IsLogged)
                        {
                            Connect();
                            
                        }
                        else
                        {
                            MessageBox.Show("Utilisateur inconnu", "Erreur",
                                                      MessageBoxButtons.OK,
                                                     MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("L'adresses email saisie n'a pas de compte associé sur Outlook", "Erreur",
                                                     MessageBoxButtons.OK,
                                                     MessageBoxIcon.Error);
                    }
                }
            }

            catch (Exception)
            {
                MessageBox.Show("adresse email incorrecte", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Connect()
        {
            Cursor.Current = Cursors.WaitCursor;
            api.synchronizeAgendas();
            Globals.ThisAddIn.GoTo("Organiser un rendez-vous");
            Cursor.Current = Cursors.Default;
            tabControl1.SelectedIndex = 2;
        }
        #endregion
        #region menu page
        private void initMenuPage()
        {
            button9.BackColor = GladysStyle.GreenButtonColor();
            button7.BackColor = GladysStyle.RedButtonColor();
            label5.Text = data.Email;
            if (data.IsLogged)
            {
                JObject o = JObject.Parse(data.dataUser);
                textBox4.Text = (string)o["personalUri"];
            }
        }
        private void button7_Click(object sender, EventArgs e)
        {
            data.IsLogged = false;
            tabControl1.SelectedIndex = 0;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 3;

        }
        #endregion
        #region sharing
        private void initSharingPage()
        {
            button10.BackColor = GladysStyle.GreenButtonColor();
            button11.BackColor = GladysStyle.GreenButtonColor();
        }
        private void button12_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 2;

        }

        private void button11_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textBox4.Text);

        }
        #endregion
        #region meeting page description
        private bool _canUpdate = true;
        private bool _needUpdate = false;
        private void initMeetingDescriptionPage()
        {
            textBox6.DataBindings.Add("Text", data.meeting, "title");
            textBox5.DataBindings.Add("Text", data.meeting, "description");
            comboBox1.DataBindings.Add("Text", data.meeting, "place");
            button13.BackColor = GladysStyle.GreenButtonColor();
        }
        private void button13_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBox6.Text))
                MessageBox.Show("Vous devez renseigner au moins le titre", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else tabControl1.SelectedIndex = 5;

        }
        private void button14_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 2;

        }

        private void button16_Click(object sender, EventArgs e)
        {
            updateApiDatas();
            tabControl1.SelectedIndex = 4;

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            _canUpdate = true;
            timer1.Stop();
            UpdateData();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            _needUpdate = false;
        }
        private void comboBox1_TextUpdate(object sender, EventArgs e)
        {
            _needUpdate = true;
        }
        //While timer is running don't start search
        //timer1.Interval = 1500;
        private void RestartTimer()
        {
            timer1.Stop();
            _canUpdate = false;
            timer1.Start();
        }
        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            if (_needUpdate)
            {
                if (_canUpdate)
                {
                    _canUpdate = false;
                    UpdateData();
                }
                else
                {
                    RestartTimer();
                }
            }
        }
        private void UpdateData()
        {
            if (comboBox1.Text.Length > 1)
            {
                List<string> searchData = GooGlePlaceAutocomplete.get(comboBox1.Text);
                HandleTextChanged(searchData);
            }
        }
        private void HandleTextChanged(List<string> dataSource)
        {
            return;  // Remove Google Place
            var text = comboBox1.Text;
            if (dataSource.Count > 0)
            {
                comboBox1.DataSource = dataSource;
                var sText = comboBox1.Items[0].ToString();
                comboBox1.SelectionStart = text.Length;
                comboBox1.SelectionLength = sText.Length - text.Length;
                comboBox1.DroppedDown = true;
                Cursor.Current = Cursors.Default;
            }
            else
            {
                comboBox1.DroppedDown = false;
                comboBox1.SelectionStart = text.Length;
            }
        }
        #endregion
        #region meeting page moments
        private OrderedDictionary MomentDict = new OrderedDictionary();
        private OrderedDictionary DurationDict = new OrderedDictionary();
        private OrderedDictionary PeriodDict = new OrderedDictionary();
        private void initMeetingMomentsPage()
        {
            MomentDict.Add("Aux heures de bureaux (de 9H a 18H)", "every-time");
            MomentDict.Add("Petit-déjeuner (de 8H a 10H)", "breakfast");
            MomentDict.Add("Matinée (de 9H a 12H)", "morning");
            MomentDict.Add("Déjeuner (de 12H30 a 14H)", "lunch");
            MomentDict.Add("Après midi (de 14H a 18H)", "afternoon");
            MomentDict.Add("Dîner (de 20H30 a 22H)", "dinner");
            MomentDict.Add("Fin d'après midi et diner (de 17H a 22H)", "end-afternoon");
            MomentDict.Add("Fin de matinée et déjeuner (de 11H a 14H)", "end-morning");
            MomentDict.Add("Soirée (de 20H a 00H)", "evening");
            foreach (DictionaryEntry de in MomentDict)
            {
                comboBox4.Items.Add(de.Key);
            }
            comboBox4.SelectedIndex = 0;
            DurationDict.Add("30 min", 0.5);
            DurationDict.Add("1 h 00 min", 1.0);
            DurationDict.Add("1 h 30 min", 1.5);
            DurationDict.Add("2 h 00 min", 2.0);
            DurationDict.Add("2 h 30 min", 2.5);
            DurationDict.Add("Demi-journée", 3.0);
            DurationDict.Add("Journée", 9.0);
            foreach (DictionaryEntry de in DurationDict)
            {
                comboBox2.Items.Add(de.Key);
            }
            comboBox2.SelectedIndex = 0;

            PeriodDict.Add("Dès que possible", "asap");
            PeriodDict.Add("Avant le", "before");
            PeriodDict.Add("Après le", "after");
            PeriodDict.Add("Date précise", "precise-date");
            foreach (DictionaryEntry de in PeriodDict)
            {
                comboBox3.Items.Add(de.Key);
            }
            comboBox3.SelectedIndex = 0;

            dateTimePicker1.MinDate = DateTime.Now.AddDays(1);
            button15.BackColor = GladysStyle.GreenButtonColor();
        }
        private void button9_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 4;
        }

        private void button15_Click(object sender, EventArgs e)
        {
            updateApiDatas();
            Cursor.Current = Cursors.WaitCursor;
            data.allContacts = OutlookContactsHelper.extract(); ;
            if (data.allContacts != null)
            {
                fillContacts();
                Cursor.Current = Cursors.Default;
                tabControl1.SelectedIndex = 6;
            }
            
        }
        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            showDateTime((comboBox3.SelectedIndex == 0) ? false : true);
        }
        private void showDateTime(bool visible)
        {
            dateTimePicker1.Visible = visible;
            label12.Visible = visible;
        }
        private void updateApiDatas()
        {
            var meeting = Globals.ThisAddIn.api.data.meeting;
            string moment = comboBox1.GetItemText(comboBox4.SelectedItem);
            meeting.moment = (string)MomentDict[moment];
            string duration = comboBox2.GetItemText(comboBox2.SelectedItem);
            double t = (double)DurationDict[duration] * 60;
            meeting.expectedTime = ((double)DurationDict[duration] * 60).ToString();
            string period = comboBox3.GetItemText(comboBox3.SelectedItem);
            meeting.period = (string)PeriodDict[period];
            meeting.date = dateTimePicker1.Value.ToString("yyyy-MM-dd");
        }

        #endregion
        #region page meeting contacts
        private void initMeetingContactsPage()
        {
            Valider.BackColor = GladysStyle.GreenButtonColor();
            button17.Enabled = false;
        }
        private void button18_Click(object sender, EventArgs e)
        {
             tabControl1.SelectedIndex = 5;

        }

        private void button23_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 6;

        }
        public void fillContacts()
        {
            checkedListBox1.ClearSelected();
            foreach (var c in data.allContacts)
            {
                if (c.Email != data.Email)
                    checkedListBox1.Items.Add(c.Email);
            }
            button16.Enabled = false;
            textBox7.Text = "";
        }


        private void button24_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 6;

        }

        private void Valider_Click(object sender, EventArgs e)
        {
            data.contacts.Clear();
            if (checkedListBox1.CheckedItems.Count > 0)
            {
                Cursor.Current = Cursors.WaitCursor;
                foreach (var c in checkedListBox1.CheckedItems)
                {
                    Contact ct = new Contact();
                    ct.Email = c.ToString();
                    data.contacts.Add(ct);
                }
                data.meeting.ownerId = data.UserId.ToString();
                bool error = api.CreateMeeting();
                Cursor.Current = Cursors.Default;
                if (error)
                {
                    tabControl1.SelectedIndex = 2;
                }
                else tabControl1.SelectedIndex = 7;
            }
            else
            {
                MessageBox.Show("Vous n'avez sélectionné personne", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void button17_Click(object sender, EventArgs e)
        {
            try
            {
                MailAddress email = new MailAddress(textBox7.Text);
                int res = checkedListBox1.FindString(email.Address);
                if (res >= 0)
                {
                    // this email is already in the contacts, we select it
                    checkedListBox1.SetItemChecked(res, true);
                    checkedListBox1.SetSelected(res, true);
                }
                else
                {
                    bool ok = api.addContact(email.Address);
                    if (ok)
                    {
                        checkedListBox1.SetSelected(checkedListBox1.Items.Add(email.Address, true), true);
                        OutlookContactsHelper.CreateContact(email.Address);
                    }
                }
                textBox7.Text = "";
            }
            catch (Exception)
            {
                MessageBox.Show("adresse email incorrecte", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void textBox7_TextChanged(object sender, EventArgs e)
        {
            try
            {
                MailAddress email = new MailAddress(textBox7.Text);
                button17.Enabled = true;
            }
            catch (Exception)
            {
                button17.Enabled = false;
            }

        }
        #endregion
        #region timeslots page 
        private void initTimeSlotsPage()
        {
            api.TimeSlotsFound += timeSlotsFound;
            button19.BackColor = GladysStyle.GreenButtonColor();
            button20.BackColor = GladysStyle.GreenButtonColor();
            button21.BackColor = GladysStyle.GreenButtonColor();
            button22.BackColor = GladysStyle.GreenButtonColor();
        }

        public void timeSlotsFound(object sender, EventArgs e)
        {
            button22.Text = data.timeSlots[0].ToString();
            button21.Text = data.timeSlots[1].ToString();
            button20.Text = data.timeSlots[2].ToString();

        }
        private void button22_Click(object sender, EventArgs e)
        {
            bool cancel = api.selectTimeSlot(data.timeSlots[0]);
            if (!cancel)
                tabControl1.SelectedIndex = 9;
        }
        private void button21_Click(object sender, EventArgs e)
        {
            bool cancel = api.selectTimeSlot(data.timeSlots[0]);
            if (!cancel)
                tabControl1.SelectedIndex = 9;
        }
        private void button20_Click(object sender, EventArgs e)
        {
            bool cancel = api.selectTimeSlot(data.timeSlots[0]);
            if (!cancel)
                tabControl1.SelectedIndex = 9;
        }
        private void button19_Click(object sender, EventArgs e)
        {
            OnLoadMoreTimeSlots();
            tabControl1.SelectedIndex = 8;

        }
        #endregion
        #region more timeSlots
        private int index = 3;
        private void initMoreTimeSlotsPage()
        {
            button29.Visible = true;
            button28.Visible = true;
            button27.Visible = true;
            button29.BackColor = GladysStyle.GreenButtonColor();
            button28.BackColor = GladysStyle.GreenButtonColor();
            button27.BackColor = GladysStyle.GreenButtonColor();
        }
        public void OnLoadMoreTimeSlots()
        {
            index = 3;
            updateButtons();
        }
        private void updateButtons()
        {
            if (index < data.timeSlots.Count)
            {
                button29.Visible = true;
                button29.Text = data.timeSlots[index].ToString();
            }
            else
            {
                button29.Visible = false;
            }

            if (index < data.timeSlots.Count - 1)
            {
                button28.Visible = true;
                button28.Text = data.timeSlots[index + 1].ToString();
            }
            else
            {
                button28.Visible = false;
            }
            if (index < data.timeSlots.Count - 2)
            {
                button27.Visible = true;
                button27.Text = data.timeSlots[index + 1].ToString();
            }
            else
            {
                button27.Visible = false;
            }

        }
        private void button29_Click(object sender, EventArgs e)
        {
            bool cancel = api.selectTimeSlot(data.timeSlots[index]);
            if (!cancel)
                tabControl1.SelectedIndex = 9;
        }
        private void button28_Click(object sender, EventArgs e)
        {
            bool cancel = api.selectTimeSlot(data.timeSlots[index+1]);
            if (!cancel)
                tabControl1.SelectedIndex = 9;
        }
        private void button27_Click(object sender, EventArgs e)
        {
            bool cancel = api.selectTimeSlot(data.timeSlots[index + 2]);
            if (!cancel)
                tabControl1.SelectedIndex = 9;
        }
        private void button26_Click(object sender, EventArgs e)
        {
            // plus avant
            Cursor.Current = Cursors.WaitCursor;
            if (index <= 0)
            {
                api.moreTimeSlotsBefore();
                index = 0;
            }
            else
            {
                index -= 3;
            }
            updateButtons();
            Cursor.Current = Cursors.Default;
        }
        private void button25_Click(object sender, EventArgs e)
        {
            // Plus après
            Cursor.Current = Cursors.WaitCursor;
            if (index + 3 >= data.timeSlots.Count - 3)
            {
                api.moreTimeSlotsAfter();
            }
            index += 3;
            updateButtons();
            Cursor.Current = Cursors.Default;
        }
        #endregion
        #region page meeting created
        private void initMeetingCreatedPage()
        {
            button32.BackColor = GladysStyle.RedButtonColor();
            button31.BackColor = GladysStyle.GreenButtonColor();
        }
        private void button32_Click(object sender, EventArgs e)
        {
            api.sendMailsByGladys();
            tabControl1.SelectedIndex = 2;
        }

        private void button31_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 2;
        }

        private void button30_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 2;
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace OutlookVstoAddIn
{
    public class Contact : IEquatable<Contact>
    {
        public string Account { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string Email { get; set; }
        public int id { get; set; }
        public bool mandatory { get; set; }
        public bool Equals(Contact other)
        {

            //Check whether the compared object is null. 
            if (Object.ReferenceEquals(other, null)) return false;

            //Check whether the compared object references the same data. 
            if (Object.ReferenceEquals(this, other)) return true;

            //Check whether the products' properties are equal. 
            return Email.Equals(other.Email);
        }
        public override int GetHashCode()
        {
            //Get hash code for the Name field if it is not null. 
            return Email == null ? 0 : Email.GetHashCode();
        }
    }
    // retrieve all the Outlook contacts with the account associated
    public class OutlookContactsHelper
    {
        #region private methods
        // Uses recursion to enumerate Outlook subfolders.
        static private void EnumerateContacts(Outlook.Folder folder, List<Contact> contacts, string account)
        {
            foreach (Object o in folder.Items)
            {
                Outlook.ContactItem contact = o as Outlook.ContactItem;
                if (contact == null)
                {
                    // it's unnecessary to continue if the item isn't a contact
                    break;
                }
                else
                {
                    if (contact.Email1Address != null)
                    {
                        Contact c = new Contact();
                        c.Account = account;
                        c.Email = contact.Email1Address;
                        c.FirstName = contact.FirstName;
                        c.LastName = contact.LastName;
                        contacts.Add(c);
                    }
                }
            }
            Outlook.Folders childFolders = folder.Folders;
            foreach (Outlook.Folder childFolder in childFolders)
            {
                // Call EnumerateFolders using childFolder.
                EnumerateContacts(childFolder, contacts, account);
            }
        }
        static private Outlook.MAPIFolder GetContactsFolder()
        {
            Outlook.NameSpace session = Globals.ThisAddIn.Application.Session;
            MyGladysApi api = Globals.ThisAddIn.api;
            Outlook.MAPIFolder contactsFolder = null;
            try
            {
                if (api.data.IsLogged)
                {
                    foreach (Outlook.Account account in session.Accounts)
                    {
                        if (account.SmtpAddress == api.data.Email)
                        {
                            contactsFolder = account.DeliveryStore.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderContacts);
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Erreur dans getContactsFolder", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return contactsFolder;
        }
        #endregion
        #region public static meyhods
        static public List<Contact> extract()
        {
            try
            {
                List<Contact> _contacts = new List<Contact>();
                Outlook.NameSpace session = Globals.ThisAddIn.Application.Session;
                foreach (Outlook.Account account in session.Accounts)
                {
                    foreach (Outlook.Folder folder in session.Folders)
                    {
                        if (folder.StoreID == account.DeliveryStore.StoreID)
                        {
                            EnumerateContacts(folder, _contacts, account.SmtpAddress);
                        }
                    }
                }
                // only Distincts (we must remove duplicates)
                _contacts = _contacts.Distinct().ToList();
                // sorted on Email
                _contacts = _contacts.OrderBy(o => o.Email).ToList();
                return _contacts;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Erreur dans extract", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }
        static public void CreateContact(string email)
        {
            try
            {
                Outlook.MAPIFolder contactsFolder = GetContactsFolder();
                if (contactsFolder != null)
                {
                    var items = contactsFolder.Items;
                    Outlook.ContactItem newContact = items.Add(Outlook.OlItemType.olContactItem) as Outlook.ContactItem;
                    newContact.Email1Address = email;
                    newContact.Save();
                }
                else
                {
                    MessageBox.Show("Impossible de trouver le dossier contacts", "Erreur dans CreateContact", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Erreur dans CreateContact", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion
    }
}

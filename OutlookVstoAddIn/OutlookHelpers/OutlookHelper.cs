﻿using Outlook = Microsoft.Office.Interop.Outlook;

namespace OutlookVstoAddIn
{
    class OutlookHelper
    {
        static public bool hasLocalAccount(string email)
        {
            Outlook.NameSpace session = Globals.ThisAddIn.Application.Session;
            {
                foreach (Outlook.Account account in session.Accounts)
                    if (account.SmtpAddress == email)
                        return true;
            }
            return false;
        }
    }
}

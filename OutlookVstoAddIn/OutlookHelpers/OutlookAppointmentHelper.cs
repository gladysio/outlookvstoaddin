﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace OutlookVstoAddIn
{
    public class MeetingEvent
    {
        public string outlookId { get; set; }
        public string title { get; set; }
        public string location { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
    }
    public class OutlookAppointmentHelper
    {
        #region private methods
        static private Outlook.Folder GetCalendarFolder()
        {
            Outlook.NameSpace session = Globals.ThisAddIn.Application.Session;
            MyGladysApi api = Globals.ThisAddIn.api;
            Outlook.Folder calendarFolder = null;
            try
            {
                if (api.data.IsLogged)
                {
                    foreach (Outlook.Account account in session.Accounts)
                    {
                        if (account.SmtpAddress == api.data.Email)
                        {
                            calendarFolder = account.DeliveryStore.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderCalendar) as Outlook.Folder;
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Erreur dans getCalendarFolder", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return calendarFolder;
        }
        static private Outlook.Items GetAppointmentsInRange(
           Outlook.Folder folder, DateTime startTime, DateTime endTime)
        {
            string filter = "[Start] >= '"
                + startTime.ToString("g")
                + "' AND [End] <= '"
                + endTime.ToString("g") + "'";
            try
            {
                Outlook.Items calItems = folder.Items;
                calItems.IncludeRecurrences = true;
                calItems.Sort("[Start]", Type.Missing);
                Outlook.Items restrictItems = calItems.Restrict(filter);
                if (restrictItems.Count > 0)
                {
                    return restrictItems;
                }
                else
                {
                    return null;
                }
            }
            catch { return null; }
        }
        #endregion
        #region public static methods
        // si l'agenda lié au compte est accessible sur Internet
        // la plateforme Mygladys n'a pas besoin qu'Outlook lui fasse le travail
        static public bool IsInternetAccessibleAgenda()
        {
            string[] domainNames = { "outlook", "gmail", "live" };
            foreach (var dn in domainNames)
            {
                MyGladysApi.Data data = Globals.ThisAddIn.api.data;
                if (data.Email.ToLower().Contains(dn))
                {
                    return true;
                }
            }
            return false;
        }
        static public string AddMeetingEvent(MeetingEvent appointment)
        {
            try
            {
                if (IsInternetAccessibleAgenda())
                    return null;
                Outlook.MAPIFolder calendarFolder = GetCalendarFolder();
                if (calendarFolder != null)
                {
                    var items = calendarFolder.Items;
                    Outlook.AppointmentItem newAppointment = items.Add(Outlook.OlItemType.olAppointmentItem) as Outlook.AppointmentItem;
                    newAppointment.Start = appointment.start;
                    newAppointment.End = appointment.end;
                    newAppointment.Location = appointment.location;
                    newAppointment.AllDayEvent = false;
                    newAppointment.Subject = appointment.title;
                    newAppointment.Save();
                    return newAppointment.GlobalAppointmentID;
                }
                else
                {
                    MessageBox.Show("Impossible de trouver le calendrier", "Erreur dans AddMeetingEvent", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Erreur dans AddMeetingEvent", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return null;
        }
        // get all the future appointments in the Outlook agenda of the user  
        static public List<MeetingEvent> getAppointments()
        {
            List<MeetingEvent> events = new List<MeetingEvent>();
            try
            {
                Outlook.Folder calendarFolder = GetCalendarFolder();
                if (calendarFolder != null)
                {
                    DateTime start = DateTime.Now;
                    DateTime end = start.AddDays(100 * 365);
                    Outlook.Items rangeAppts = GetAppointmentsInRange(calendarFolder, start, end);
                    if (rangeAppts != null)
                    {
                        foreach (Outlook.AppointmentItem appt in rangeAppts)
                        {
                            events.Add(new MeetingEvent {
                                outlookId = appt.GlobalAppointmentID,
                                title = appt.Subject,
                                start = appt.Start,
                                end = appt.End
                            });
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Agenda Outlook pas trouvé", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return null;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Erreur dans getAppointments", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
            return events;
        }
        #endregion
    }
}

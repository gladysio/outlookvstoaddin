﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OutlookVstoAddIn
{
    public partial class MGSocialNetworks : UserControl
    {
        public MGSocialNetworks()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.facebook.com/MyGladys.io/");
        }
        private void button1_MouseLeave(object sender, EventArgs e)
        {
            this.button1.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.facebook_gray));
        }
        private void button1_MouseEnter(object sender, EventArgs e)
        {
            this.button1.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.facebook));
        }
        private void button2_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://fr.linkedin.com/company/gladys.io");
        }
        private void button2_MouseEnter(object sender, EventArgs e)
        {
            this.button2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.linkedin));
        }
        private void button2_MouseLeave(object sender, EventArgs e)
        {
            this.button2.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.linkedin_gray));
        }
        private void button3_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://twitter.com/MyGladys_fr");
        }
        private void button3_MouseEnter(object sender, EventArgs e)
        {
            this.button3.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.twitter));
        }
        private void button3_MouseLeave(object sender, EventArgs e)
        {
            this.button3.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.twitter_gray));
        }
        private void button4_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.instagram.com/mygladys_io/");
        }
        private void button4_MouseEnter(object sender, EventArgs e)
        {
            this.button4.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.instagram));
        }
        private void button4_MouseLeave(object sender, EventArgs e)
        {
            this.button4.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.instagram_gray));
        }
        private void button5_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.youtube.com/channel/UCo_ckcW7uD6c_jwOD3oJVOw");
        }
        private void button5_MouseEnter(object sender, EventArgs e)
        {
            this.button5.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.youtube));
        }
        private void button5_MouseLeave(object sender, EventArgs e)
        {
            this.button5.BackgroundImage = ((System.Drawing.Image)(Properties.Resources.youtube_gray));
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Change the color of the link text by setting LinkVisited   
            // to true.  
            linkLabel1.LinkVisited = true;
            //Call the Process.Start method to open the default browser   
            //with a URL:  
            System.Diagnostics.Process.Start("https://mygladys.fr");

        }
    }
}

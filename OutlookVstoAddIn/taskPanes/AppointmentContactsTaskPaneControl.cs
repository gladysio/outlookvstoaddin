﻿using System;
using System.Net.Mail;
using System.Windows.Forms;

namespace OutlookVstoAddIn
{
    public partial class AppointmentContactsTaskPaneControl : UserControl
    {
        public MyGladysApi api;
        public MyGladysApi.Data data;
        public AppointmentContactsTaskPaneControl()
        {
            api = Globals.ThisAddIn.api;
            data = Globals.ThisAddIn.api.data;
            InitializeComponent();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
            Valider.BackColor = GladysStyle.GreenButtonColor();
            button2.Enabled = false;
        }
        public void fillContacts(object sender, EventArgs e)
        {
            checkedListBox1.ClearSelected();
            foreach (var c in data.allContacts)
            {
                if (c.Email != data.Email)
                    checkedListBox1.Items.Add(c.Email);
            }
            button2.Enabled = false;
            textBox1.Text = "";
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Globals.ThisAddIn.GoTo("Quand ?");

        }

        private void Valider_Click(object sender, EventArgs e)
        {
            data.contacts.Clear();
            if (checkedListBox1.CheckedItems.Count > 0)
            {
                Cursor.Current = Cursors.WaitCursor;
                foreach (var c in checkedListBox1.CheckedItems)
                {
                    Contact ct = new Contact();
                    ct.Email = c.ToString();
                    data.contacts.Add(ct);
                }
                data.meeting.ownerId = data.UserId.ToString();
                bool error = api.CreateMeeting();
                Cursor.Current = Cursors.Default;
                if (error)
                {
                    Globals.ThisAddIn.GoTo("Organiser un rendez-vous");
                }
                else Globals.ThisAddIn.GoTo("Créneaux trouvés");
            }
            else
            {
                MessageBox.Show("Vous n'avez sélectionné personne", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                MailAddress email = new MailAddress(textBox1.Text);
                int res=checkedListBox1.FindString(email.Address);
                if (res>=0)
                {
                    // this email is already in the contacts, we select it
                    checkedListBox1.SetItemChecked(res, true);
                    checkedListBox1.SetSelected(res, true);
                 }
                else
                {
                    bool ok=api.addContact(email.Address);
                    if (ok)
                    {
                        checkedListBox1.SetSelected(checkedListBox1.Items.Add(email.Address, true), true);
                        OutlookContactsHelper.CreateContact(email.Address);
                    }
                 }
                textBox1.Text = "";
            }
            catch (Exception)
            {
                MessageBox.Show("adresse email incorrecte", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                MailAddress email = new MailAddress(textBox1.Text);
                button2.Enabled = true;
            }
            catch (Exception)
            {
                button2.Enabled = false;
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using System.Windows.Forms;

namespace OutlookVstoAddIn
{
    public partial class AppointmentMomentTaskPaneControl : UserControl
    {
        private OrderedDictionary MomentDict = new OrderedDictionary();
        private OrderedDictionary DurationDict = new OrderedDictionary();
        private OrderedDictionary PeriodDict = new OrderedDictionary();
        public MyGladysApi.Data data;
        public MyGladysApi api;
        public event EventHandler FillContacts;

        public AppointmentMomentTaskPaneControl()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
            data = Globals.ThisAddIn.api.data;
            api = Globals.ThisAddIn.api;

            MomentDict.Add("Aux heures de bureaux (de 9H a 18H)", "every-time");
            MomentDict.Add("Petit-déjeuner (de 8H a 10H)", "breakfast");
            MomentDict.Add("Matinée (de 9H a 12H)", "morning");
            MomentDict.Add("Déjeuner (de 12H30 a 14H)", "lunch");
            MomentDict.Add("Après midi (de 14H a 18H)", "afternoon");
            MomentDict.Add("Dîner (de 20H30 a 22H)", "dinner");
            MomentDict.Add("Fin d'après midi et diner (de 17H a 22H)", "end-afternoon");
            MomentDict.Add("Fin de matinée et déjeuner (de 11H a 14H)", "end-morning");
            MomentDict.Add("Soirée (de 20H a 00H)", "evening");
            foreach (DictionaryEntry de in MomentDict)
            {
                comboBox1.Items.Add(de.Key);
            }
            comboBox1.SelectedIndex=0;
            DurationDict.Add("30 min", 0.5);
            DurationDict.Add("1 h 00 min", 1.0);
            DurationDict.Add("1 h 30 min", 1.5);
            DurationDict.Add("2 h 00 min", 2.0);
            DurationDict.Add("2 h 30 min", 2.5);
            DurationDict.Add("Demi-journée", 3.0);
            DurationDict.Add("Journée", 9.0);
            foreach (DictionaryEntry de in DurationDict)
            {
                comboBox2.Items.Add(de.Key);
            }
            comboBox2.SelectedIndex=0;

            PeriodDict.Add("Dès que possible", "asap");
            PeriodDict.Add("Avant le", "before");
            PeriodDict.Add("Après le", "after");
            PeriodDict.Add("Date précise", "precise-date");
            foreach (DictionaryEntry de in PeriodDict)
            {
                comboBox3.Items.Add(de.Key);
            }
            comboBox3.SelectedIndex = 0;

            dateTimePicker1.MinDate = DateTime.Now.AddDays(1);
            button2.BackColor = GladysStyle.GreenButtonColor();

        }
        private void showDateTime(bool visible)
        {
            dateTimePicker1.Visible = visible;
            label4.Visible = visible;
        }
        private void updateApiDatas()
        {
            var meeting = Globals.ThisAddIn.api.data.meeting;
            string moment = comboBox1.GetItemText(comboBox1.SelectedItem);
            meeting.moment = (string)MomentDict[moment];
            string duration = comboBox2.GetItemText(comboBox2.SelectedItem);
            double t = (double)DurationDict[duration]*60;
            meeting.expectedTime = ((double)DurationDict[duration] * 60).ToString();
            string period = comboBox3.GetItemText(comboBox3.SelectedItem);
            meeting.period = (string)PeriodDict[period];
            meeting.date = dateTimePicker1.Value.ToString("yyyy-MM-dd"); 
        }


        private void button1_Click(object sender, EventArgs e)
        {
            updateApiDatas();
            Globals.ThisAddIn.GoTo("Ou et quoi ?");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            updateApiDatas();
            Cursor.Current = Cursors.WaitCursor;
            data.allContacts = OutlookContactsHelper.extract(); ;
            if (data.allContacts != null)
            {
                OnFillContacts(EventArgs.Empty);
                Cursor.Current = Cursors.Default;
                Globals.ThisAddIn.GoTo("Avec qui ?");
            }
        }
        protected virtual void OnFillContacts(EventArgs e)
        {
            EventHandler handler = FillContacts;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            showDateTime((comboBox3.SelectedIndex == 0) ? false : true);
<<<<<<< HEAD

=======
>>>>>>> parent of 1103293... cleaning
        }
    }
}

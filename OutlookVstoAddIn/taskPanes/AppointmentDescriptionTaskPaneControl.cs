﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace OutlookVstoAddIn
{
    public partial class AppointmentDescriptionTaskPaneControl : UserControl
    {
        private bool _canUpdate = true;
        private bool _needUpdate = false;
        public MyGladysApi.Data data;
        public AppointmentDescriptionTaskPaneControl()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
            data = Globals.ThisAddIn.api.data;
            MyGladysApi.Meeting meeting = data.meeting;
            textBox1.DataBindings.Add("Text", meeting, "title");
            textBox3.DataBindings.Add("Text", meeting, "description");
            comboBox1.DataBindings.Add("Text", meeting, "place");
            button2.BackColor = GladysStyle.GreenButtonColor();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Globals.ThisAddIn.GoTo("Organiser un rendez-vous");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBox1.Text))
                MessageBox.Show("Vous devez renseigner au moins le titre", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else Globals.ThisAddIn.GoTo("Quand ?");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            _canUpdate = true;
            timer1.Stop();
            UpdateData();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            _needUpdate = false;
        }

        private void comboBox1_TextUpdate(object sender, EventArgs e)
        {
            _needUpdate = true;
        }
        //While timer is running don't start search
        //timer1.Interval = 1500;
        private void RestartTimer()
        {
            timer1.Stop();
            _canUpdate = false;
            timer1.Start();
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            if (_needUpdate)
            {
                if (_canUpdate)
                {
                    _canUpdate = false;
                    UpdateData();
                }
                else
                {
                    RestartTimer();
                }
            }
        }
        private void UpdateData()
        {
            if (comboBox1.Text.Length > 1)
            {
                List<string> searchData = GooGlePlaceAutocomplete.get(comboBox1.Text);
                HandleTextChanged(searchData);
            }
        }
        private void HandleTextChanged(List<string> dataSource)
        {
            var text = comboBox1.Text;
            if (dataSource.Count > 0)
            {
                comboBox1.DataSource = dataSource;
                var sText = comboBox1.Items[0].ToString();
                comboBox1.SelectionStart = text.Length;
                comboBox1.SelectionLength = sText.Length - text.Length;
                comboBox1.DroppedDown = true;
                Cursor.Current = Cursors.Default;
            }
            else
            {
                comboBox1.DroppedDown = false;
                comboBox1.SelectionStart = text.Length;
            }
        }
    }
}

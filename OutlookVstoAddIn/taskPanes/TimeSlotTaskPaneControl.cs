﻿using System;
using System.Windows.Forms;

namespace OutlookVstoAddIn
{
    public partial class TimeSlotTaskPaneControl : UserControl
    {
        public MyGladysApi.Data data;
        public MyGladysApi api;
        public event EventHandler LoadMoreTimeSlots;
        public TimeSlotTaskPaneControl()
        {
            data = Globals.ThisAddIn.api.data;
            api = Globals.ThisAddIn.api;
            InitializeComponent();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
            api.TimeSlotsFound += timeSlotsFound;
            button4.BackColor = GladysStyle.GreenButtonColor();
            button5.BackColor = GladysStyle.GreenButtonColor();
            button6.BackColor = GladysStyle.GreenButtonColor();
            button7.BackColor = GladysStyle.GreenButtonColor();
            button8.BackColor = GladysStyle.GreenButtonColor();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Globals.ThisAddIn.GoTo("Avec qui ?");
        }

        private void listBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }



        public void timeSlotsFound(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            foreach (MyGladysApi.TimeSlot ts in data.timeSlots)
            {
                listBox1.Items.Add(ts);
            }
            button5.Text = data.timeSlots[0].ToString();
            button6.Text = data.timeSlots[1].ToString();
            button7.Text = data.timeSlots[2].ToString();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            api.moreTimeSlotsBefore();
        }

        private void button3_Click(object sender, EventArgs e)
        {

            api.moreTimeSlotsAfter();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MyGladysApi.TimeSlot sel = (MyGladysApi.TimeSlot)listBox1.SelectedItem;
            if (sel != null)
            {
                api.selectTimeSlot(sel);
                Globals.ThisAddIn.GoTo("Rendez-vous créé");
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            bool cancel = api.selectTimeSlot(data.timeSlots[0]);
            if (!cancel)
                Globals.ThisAddIn.GoTo("Rendez-vous créé");

        }

        private void button6_Click(object sender, EventArgs e)
        {
            bool cancel = api.selectTimeSlot(data.timeSlots[1]);
            if (!cancel)
                Globals.ThisAddIn.GoTo("Rendez-vous créé");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            bool cancel = api.selectTimeSlot(data.timeSlots[2]);
            if (!cancel)
                Globals.ThisAddIn.GoTo("Rendez-vous créé");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            OnLoadMoreTimeSlots(EventArgs.Empty);
            Globals.ThisAddIn.GoTo("Plus de créneaux");
        }
        protected virtual void OnLoadMoreTimeSlots(EventArgs e)
        {
            EventHandler handler = LoadMoreTimeSlots;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}

﻿using System;
using System.Windows.Forms;

namespace OutlookVstoAddIn
{
    public partial class HomeTaskPaneControl : UserControl
    {
        public HomeTaskPaneControl()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
            button1.BackColor = GladysStyle.GreenButtonColor();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Globals.ThisAddIn.GoTo("Connection");
        }
    }
}

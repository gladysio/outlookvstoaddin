﻿using System;
using System.Windows.Forms;

namespace OutlookVstoAddIn
{
    public partial class AppointmentCreatedTaskPaneControl : UserControl
    {
        public MyGladysApi.Data data;
        public MyGladysApi api;
        public AppointmentCreatedTaskPaneControl()
        {
            data = Globals.ThisAddIn.api.data;
            api = Globals.ThisAddIn.api;
            InitializeComponent();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
            button1.BackColor = GladysStyle.RedButtonColor();
            button2.BackColor = GladysStyle.GreenButtonColor();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            api.sendMailsByGladys();
            Globals.ThisAddIn.GoTo("Organiser un rendez-vous");

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Globals.ThisAddIn.GoTo("Organiser un rendez-vous");

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Globals.ThisAddIn.GoTo("Organiser un rendez-vous");

        }
    }
}

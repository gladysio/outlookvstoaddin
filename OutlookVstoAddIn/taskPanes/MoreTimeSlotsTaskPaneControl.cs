﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OutlookVstoAddIn
{
    public partial class MoreTimeSlotsTaskPaneControl : UserControl
    {
        public MyGladysApi.Data data;
        public MyGladysApi api;
        private int index = 3;
        public MoreTimeSlotsTaskPaneControl()
        {
            data = Globals.ThisAddIn.api.data;
            api = Globals.ThisAddIn.api;
            InitializeComponent();
            button5.BackColor = GladysStyle.GreenButtonColor();
            button1.BackColor = GladysStyle.GreenButtonColor();
            button2.BackColor = GladysStyle.GreenButtonColor();
        }
        public void onLoad(object sender, EventArgs e)
        {
            index = 3;
            updateButtons();
        }
        private void updateButtons()
        {
            if (index<data.timeSlots.Count)
                button5.Text = data.timeSlots[index].ToString();
            if (index < data.timeSlots.Count-1)
                button1.Text = data.timeSlots[index + 1].ToString();
            if (index < data.timeSlots.Count - 2)
                button2.Text = data.timeSlots[index + 2].ToString();

        }
        private void button6_Click(object sender, EventArgs e)
        {
            Globals.ThisAddIn.GoTo("Avec qui ?");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            // 1
            bool cancel=api.selectTimeSlot(data.timeSlots[index]);
            if (!cancel)
                Globals.ThisAddIn.GoTo("Rendez-vous créé");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // 2
            bool cancel = api.selectTimeSlot(data.timeSlots[index+1]);
            if (!cancel)
                Globals.ThisAddIn.GoTo("Rendez-vous créé");
         }

        private void button2_Click(object sender, EventArgs e)
        {
            // 3
            bool cancel = api.selectTimeSlot(data.timeSlots[index + 2]);
            if (!cancel)
                Globals.ThisAddIn.GoTo("Rendez-vous créé");

        }

        private void button3_Click(object sender, EventArgs e)
        {
            // plus avant
            if (index <= 0)
            {
                api.moreTimeSlotsBefore();
                index = 0;
            }
            else
            {
                index -= 3;
            }
            updateButtons();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            // Plus après
            if (index + 3 >= data.timeSlots.Count-3)
            {
                api.moreTimeSlotsAfter();
            }
            index += 3;
            updateButtons();
        }
<<<<<<< HEAD
=======

        private void MoreTimeSlotsTaskPaneControl_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
>>>>>>> parent of 1103293... cleaning
    }
}

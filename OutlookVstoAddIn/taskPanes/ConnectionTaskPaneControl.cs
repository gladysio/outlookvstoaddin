﻿using System;
using System.Windows.Forms;

namespace OutlookVstoAddIn
{
    public partial class ConnectionTaskPaneControl : UserControl
    {
        public MyGladysApi api;
        public MyGladysApi.Data data;
        public event EventHandler UserChange;
        public ConnectionTaskPaneControl()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);

            data = Globals.ThisAddIn.api.data;
            api = Globals.ThisAddIn.api;
            button1.BackColor = GladysStyle.GreenButtonColor();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                if (OutlookHelper.hasLocalAccount(textBox1.Text))
                {
                    data.Email = textBox1.Text;
                    data.Password = textBox2.Text;
                    api.Login();
                    OnUserChange(EventArgs.Empty);
                    textBox1.Text = "";
                    textBox2.Text = "";
                    if (data.IsLogged)
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        api.synchronizeAgendas();
                        Globals.ThisAddIn.GoTo("Organiser un rendez-vous");
                        Cursor.Current = Cursors.Default;
                    }
                    else
                    {
                        MessageBox.Show("Utilisateur inconnu", "Erreur",
                                                     MessageBoxButtons.OK,
                                                     MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("L'adresses email saisie n'a pas de compte associé sur Outlook", "Erreur",
                                                 MessageBoxButtons.OK,
                                                 MessageBoxIcon.Error);

                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "fabrice.mygladys@outlook.fr";
            textBox2.Text = "5f6ae9c80966f695c84b64dceabb6db7f88755f2";
            textBox2.Text = "1cbda394ef04a2ea54362cc527a62124db550dbb";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = "mygladys@groupe-pegasus.com";
            textBox2.Text = "der58gvb69!";
        }
        protected virtual void OnUserChange(EventArgs e)
        {
            EventHandler handler = UserChange;
            if (handler != null)
            {
                handler(this, e);
            }
        }

    }
}

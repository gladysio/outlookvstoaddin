﻿using System;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace OutlookVstoAddIn
{
    public partial class DisponibilitiesSharingTaskPaneControl : UserControl
    {
        public MyGladysApi.Data data;
        public DisponibilitiesSharingTaskPaneControl()
        {
            data = Globals.ThisAddIn.api.data;
            InitializeComponent();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
            if (data.IsLogged)
            {
                JObject o = JObject.Parse(data.dataUser);
                textBox1.Text = (string)o["personalUri"];
            }
            button2.BackColor = GladysStyle.GreenButtonColor();
            button3.BackColor = GladysStyle.GreenButtonColor();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Globals.ThisAddIn.GoTo("Organiser un rendez-vous");
         }

        private void button2_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textBox1.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }
    }
}

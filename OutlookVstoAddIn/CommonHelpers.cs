﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace OutlookVstoAddIn
{
    public static class CommonHelpers
    {
        public static string GetExecutionPath()
        {
            //Get the assembly information
            System.Reflection.Assembly assemblyInfo = System.Reflection.Assembly.GetExecutingAssembly();

            //Location is where the assembly is run from 
            string assemblyLocation = assemblyInfo.Location;

            //CodeBase is the location of the ClickOnce deployment files
            Uri uriCodeBase = new Uri(assemblyInfo.CodeBase);
            return Path.GetDirectoryName(uriCodeBase.LocalPath.ToString());

        }

        /// <summary>
        /// Serialize xml file.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="file"></param>
        /// <param name="path"></param>
        public static void SaveIntoXmlFile<T>(T objectToSeriliaze, String path)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            using (StreamWriter writer = new StreamWriter(path))
            {
                serializer.Serialize(writer, objectToSeriliaze);
            }
        }

        /// <summary>
        /// Loads xml data into an object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlFile"></param>
        /// <returns></returns>
        public static T LoadXMLFileIntoClass<T>(string xmlFile)
            where T: new()
        {
            T returnThis;
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            if (!File.Exists(xmlFile))
            {
                return new T();
            }
            returnThis = (T)serializer.Deserialize(new StreamReader(xmlFile));
            return (T)returnThis;
        }

    }
}

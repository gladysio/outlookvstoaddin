﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Tools;


// This object manage a set of task panes (pages) and manage the visibility and the navigation

namespace OutlookVstoAddIn
{
    public class TaskPaneManager
    {
        private HomeTaskPaneControl homeTaskPaneControl;
        private ConnectionTaskPaneControl connectionTaskPaneControl;
        private MenuTaskPaneControl menuTaskPaneControl;
        private DisponibilitiesSharingTaskPaneControl disponibilitiesSharingTaskPaneControl;
        private AppointmentDescriptionTaskPaneControl appointmentDescriptionTaskPaneControl;
        private AppointmentMomentTaskPaneControl appointmentMomentTaskPaneControl;
        private AppointmentContactsTaskPaneControl appointmentContactsTaskPaneControl;
        private AppointmentCreatedTaskPaneControl appointmentCreatedTaskPaneControl;
        private TimeSlotTaskPaneControl timeSlotTaskPaneControl;
        private MoreTimeSlotsTaskPaneControl moreTimeSlotsTaskPaneControl;



        private CustomTaskPane currentPane=null;
        private List<CustomTaskPane> taskPanes;

        // In the constructor, we instanciate all the pages.
        public TaskPaneManager()
        {

            taskPanes = new List<CustomTaskPane>();

            homeTaskPaneControl = new HomeTaskPaneControl();
            var width = homeTaskPaneControl.Width;
            taskPanes.Add(Globals.ThisAddIn.CustomTaskPanes.Add(
                homeTaskPaneControl, "Welcome"));
            taskPanes.ElementAt(0).Width = width;
            taskPanes.ElementAt(0).VisibleChanged +=
                new EventHandler(taskPaneValue_VisibleChanged);

            connectionTaskPaneControl = new ConnectionTaskPaneControl();
            width = connectionTaskPaneControl.Width;
            taskPanes.Add(Globals.ThisAddIn.CustomTaskPanes.Add(
                connectionTaskPaneControl, "Connection"));
            taskPanes.ElementAt(1).Width = width;
            taskPanes.ElementAt(1).VisibleChanged +=
                new EventHandler(taskPaneValue_VisibleChanged);

            menuTaskPaneControl = new MenuTaskPaneControl();
            width = menuTaskPaneControl.Width;
            taskPanes.Add(Globals.ThisAddIn.CustomTaskPanes.Add(
                menuTaskPaneControl, "Organiser un rendez-vous"));
            taskPanes.ElementAt(2).Width = width;
            taskPanes.ElementAt(2).VisibleChanged +=
                new EventHandler(taskPaneValue_VisibleChanged);
            connectionTaskPaneControl.UserChange += menuTaskPaneControl.userChange;

            disponibilitiesSharingTaskPaneControl = new DisponibilitiesSharingTaskPaneControl();
            width = disponibilitiesSharingTaskPaneControl.Width;
            taskPanes.Add(Globals.ThisAddIn.CustomTaskPanes.Add(
                disponibilitiesSharingTaskPaneControl, "Partager vos disponibilités"));
            taskPanes.ElementAt(3).Width = width;
            taskPanes.ElementAt(3).VisibleChanged +=
                new EventHandler(taskPaneValue_VisibleChanged);


            appointmentDescriptionTaskPaneControl = new AppointmentDescriptionTaskPaneControl();
            width = appointmentDescriptionTaskPaneControl.Width;
            taskPanes.Add(Globals.ThisAddIn.CustomTaskPanes.Add(
                appointmentDescriptionTaskPaneControl, "Ou et quoi ?"));
            taskPanes.ElementAt(4).Width = width;
            taskPanes.ElementAt(4).VisibleChanged +=
                new EventHandler(taskPaneValue_VisibleChanged);

            appointmentMomentTaskPaneControl = new AppointmentMomentTaskPaneControl();
            width = appointmentMomentTaskPaneControl.Width;
            taskPanes.Add(Globals.ThisAddIn.CustomTaskPanes.Add(
                appointmentMomentTaskPaneControl, "Quand ?"));
            taskPanes.ElementAt(5).Width = width;
            taskPanes.ElementAt(5).VisibleChanged +=
                new EventHandler(taskPaneValue_VisibleChanged);

            appointmentContactsTaskPaneControl = new AppointmentContactsTaskPaneControl();
            width = appointmentContactsTaskPaneControl.Width;
            taskPanes.Add(Globals.ThisAddIn.CustomTaskPanes.Add(
                appointmentContactsTaskPaneControl, "Avec qui ?"));
            taskPanes.ElementAt(6).Width = width;
            taskPanes.ElementAt(6).VisibleChanged +=
                new EventHandler(taskPaneValue_VisibleChanged);

            appointmentMomentTaskPaneControl.FillContacts += appointmentContactsTaskPaneControl.fillContacts;


            appointmentCreatedTaskPaneControl = new AppointmentCreatedTaskPaneControl();
            width = appointmentCreatedTaskPaneControl.Width;
            taskPanes.Add(Globals.ThisAddIn.CustomTaskPanes.Add(
                appointmentCreatedTaskPaneControl, "Rendez-vous créé"));
            taskPanes.ElementAt(7).Width = width;
            taskPanes.ElementAt(7).VisibleChanged +=
                new EventHandler(taskPaneValue_VisibleChanged);

            timeSlotTaskPaneControl = new TimeSlotTaskPaneControl();
            width = timeSlotTaskPaneControl.Width;
            taskPanes.Add(Globals.ThisAddIn.CustomTaskPanes.Add(
                timeSlotTaskPaneControl, "Créneaux trouvés"));
            taskPanes.ElementAt(8).Width = width;
            taskPanes.ElementAt(8).VisibleChanged +=
                new EventHandler(taskPaneValue_VisibleChanged);

            moreTimeSlotsTaskPaneControl = new MoreTimeSlotsTaskPaneControl();
            width = moreTimeSlotsTaskPaneControl.Width;
            taskPanes.Add(Globals.ThisAddIn.CustomTaskPanes.Add(
                moreTimeSlotsTaskPaneControl, "Plus de créneaux"));
            taskPanes.ElementAt(9).Width = width;
            taskPanes.ElementAt(9).VisibleChanged +=
                new EventHandler(taskPaneValue_VisibleChanged);

            timeSlotTaskPaneControl.LoadMoreTimeSlots += moreTimeSlotsTaskPaneControl.onLoad;


            MyGladysApi api = Globals.ThisAddIn.api;
           if (api.data.IsLogged)
                GoTo("Organiser un rendez-vous");
            else
                GoTo("Welcome");
        }
        // Occurs when the user displays or closes the custom task pane
        // the handler below change the appearance of the toggle button in the Ribbon 
        private void taskPaneValue_VisibleChanged(object sender, System.EventArgs e)
        {

            // Si au moins 1 à Visible -> Checked à false sinon true
            bool show = false;
            try
            {
                foreach (CustomTaskPane pane in taskPanes)
                {
                    if (pane != null && pane.Visible)
                    {
                        show = true;
                        break;
                    }
                }
            }
            catch (Exception ev)
            {
                // warning this exception is thrown if Clean is not called before
                Console.WriteLine("{0} Exception caught.", ev);
            }
            Globals.Ribbons.GladysRibbon.gladysButton1.Checked = show;
        }
        // it is called when the user click on the toggle ribbon button
        public void SetTaskPaneVisible(bool visible)
        {
            currentPane.Visible = visible;
        }
        // this function remove the Event handlers when Outlook close 
        // (if we don't remove explicitly the handlers Windows send an exception)
        public void Clean()
        {
            foreach (CustomTaskPane pane in taskPanes)
            {
                pane.VisibleChanged -=
                    new EventHandler(taskPaneValue_VisibleChanged);
            }
        }
        // Find a page by his name (The title in blue in the top of the pane)       
        private CustomTaskPane FindPage(string page)
        {
            return taskPanes.Find(p => p.Title.Contains(page));
        }

        // go to page
        public void GoTo(string page,bool visible=true)
        {
            CustomTaskPane pane = FindPage(page);
            if (pane != null)
            {
                //if (currentPane != null)
                //    currentPane.Visible = !visible;
                //              taskPanes.ForEach(p => p.Visible = false);
                if (currentPane!=null)
                    currentPane.Visible = false;
                pane.Visible = visible;
                currentPane = pane;
            }
        }
    }
}

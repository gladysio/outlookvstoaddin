﻿using Outlook = Microsoft.Office.Interop.Outlook;
using OutlookInspectorWrapper;
using System.Collections.Generic;
using System;
using Microsoft.Office.Core;
using System.IO;

namespace OutlookVstoAddIn
{
    public partial class ThisAddIn
    {
        /// <summary>
        /// Holds a reference to the Application.Inspectors collection
        /// Required to get notifications for NewInspector events.
        /// </summary>
        private Outlook.Inspectors _inspectors;
        /// <summary>
        /// A dictionary that holds a reference to the Inspectors handled by the add-in
        /// </summary>
        private Dictionary<Guid, InspectorWrapper> _wrappedInspectors;
        public MyGladysApi api = new MyGladysApi();
        private MygladysTaskPane mygladysTaskPaneCtrl;
        private Microsoft.Office.Tools.CustomTaskPane pane;
        private const string USER_CONFIG_FILE_NAME = "mygladysUserConf.xml";
        private static string configFilePath = string.Empty;

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            string executionPath = CommonHelpers.GetExecutionPath();
            configFilePath = Path.Combine(executionPath, USER_CONFIG_FILE_NAME);


            if (File.Exists(configFilePath))
            {
                api.data = CommonHelpers.LoadXMLFileIntoClass<MyGladysApi.Data>(configFilePath);
                // we retrieve the auth infos from the user settings
                //api.data.IsLogged = Properties.Settings.Default.isLogged;
                //if (api.data.IsLogged)
                //{
                //    api.data.UserId = Properties.Settings.Default.userId;
                //    api.data.dataUser = Properties.Settings.Default.dataUser;
                //    api.data.Email = Properties.Settings.Default.email;
                //}
            }

            _wrappedInspectors = new Dictionary<Guid, InspectorWrapper>();
            _inspectors = Globals.ThisAddIn.Application.Inspectors;
            _inspectors.NewInspector += new Outlook.InspectorsEvents_NewInspectorEventHandler(WrapInspector);

            // Handle also already existing Inspectors
            // (e.g. Double clicking a .msg file)
            foreach (Outlook.Inspector inspector in _inspectors)
            {
                WrapInspector(inspector);
            }
            mygladysTaskPaneCtrl = new MygladysTaskPane();
            var width = mygladysTaskPaneCtrl.Width;
            pane=CustomTaskPanes.Add(mygladysTaskPaneCtrl, "MyGladys");
            pane.Width = width;
            pane.VisibleChanged +=
                new EventHandler(taskPaneValue_VisibleChanged);
            SetTaskPaneVisible(true);
                        // Fabrice -> pour nettoyer on intercepte l'évènement Quit d'Outlook
                        ((Outlook.ApplicationEvents_11_Event)Application).Quit
            += new Outlook.ApplicationEvents_11_QuitEventHandler(ThisAddIn_Quit);
        }

        private void taskPaneValue_VisibleChanged(object sender, System.EventArgs e)
        {

            Globals.Ribbons.GladysRibbon.gladysButton1.Checked = pane.Visible;
        }
        /// <summary>
        /// Wraps an Inspector if required and remember it in memory to get events of the wrapped Inspector
        /// </summary>
        /// <param name="inspector">The Outlook Inspector instance</param>
        void WrapInspector(Outlook.Inspector inspector)
        {
            InspectorWrapper wrapper = InspectorWrapper.GetWrapperFor(inspector);
            if (wrapper != null)
            {
                // register for the closed event
                wrapper.Closed += new InspectorWrapperClosedEventHandler(wrapper_Closed);
                // remember the inspector in memory
                _wrappedInspectors[wrapper.Id] = wrapper;
            }
        }
        /// <summary>
        /// Method is called when an inspector has been closed
        /// Removes reference from memory
        /// </summary>
        /// <param name="id">The unique id of the closed inspector</param>
        void wrapper_Closed(Guid id)
        {
            _wrappedInspectors.Remove(id);
        }
        public void SetTaskPaneVisible(bool visible)
        {
              pane.Visible = visible;
        }

        // if you have to clean something when Outlook close it's below
        private void Clean()
        {
             // do the homework and cleanup
            _wrappedInspectors.Clear();
            _inspectors.NewInspector -= new Outlook.InspectorsEvents_NewInspectorEventHandler(WrapInspector);
            _inspectors = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        // Remarque : Outlook ne déclenche plus cet événement. Si du code
        //    doit s'exécuter à la fermeture d'Outlook (consultez https://go.microsoft.com/fwlink/?LinkId=506785)

        // Fabrice -> cet évènement n'est plus déclenché par Microsoft à partir de Outlook 2013
        // pour qu'Outlook s'arrête plus rapidement. Cependant, l'administrateur peut paramètrer Outlook
        // pour "arrêt lent", auquel cas cet évènement est déclenché comme dans les cersions antérieures, il vaut
        // donc mieux laisser le code de nettoyage actif dans le handler ci dessous.

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            Clean();
        }
        // Fabrice -> =évènement Quit d'Outlook
        void ThisAddIn_Quit()
        {
            //Properties.Settings.Default.isLogged=api.data.IsLogged;
            //if (api.data.IsLogged)
            //{
            //    Properties.Settings.Default.userId = api.data.UserId;
            //    Properties.Settings.Default.dataUser = api.data.dataUser;
            //    Properties.Settings.Default.email = api.data.Email;
            //}
            //Properties.Settings.Default.Upgrade();
            //Properties.Settings.Default.Save();
            CommonHelpers.SaveIntoXmlFile(api.data, configFilePath);
            Clean();
        }

        #region Code généré par VSTO

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }

        public void GoTo(string page)
        {
//            taskPaneManager.GoTo(page);
        }

        #endregion
    }
}
